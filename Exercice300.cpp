#include "Exercice300.h"
#include <iostream>

using namespace std;


//constructeur par d�faut
Rectangle::Rectangle()
          : m_longueur(5), m_largeur (2)
{

}

//Constructeurs surcharg�
Rectangle::Rectangle(float _longueur, float _largeur)
          : m_longueur(_longueur), m_largeur(_largeur)

          {

          }

//destructeurs
Rectangle::~Rectangle()
{

}

//accesseurs
float Rectangle::getLongueur()
{
    return m_longueur;
}

float Rectangle::getLargeur()
{
    return m_largeur;
}

//m�thodes
float Rectangle::perimetre()
{
   float perimetre;
   perimetre = 2*getLongueur() + 2*getLargeur();
   return perimetre;
}

float Rectangle::aire()
{
    float aire;
    aire = getLongueur()*getLargeur();
    return aire;
}

void Rectangle::affichage()
{
    cout<<"La longueur du rectangle est "<<getLongueur()<<endl;
    cout<<"La largeur du rectangle est "<<getLargeur()<<endl;
    cout<<"Le perimetre du rectangle est "<<perimetre()<<endl;
    cout<<"L'aire du rectangle est "<<aire()<<endl;
}

